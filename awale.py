# Global variables
BOARD_SIZE = 12  # default cups number
INITIAL_SEEDS = 4  # initial seeds per cups
ALL_SEEDS = BOARD_SIZE * INITIAL_SEEDS

TOP_PLAYER = 1
BOTTOM_PLAYER = 0
TOP_CUPS = list(range(BOARD_SIZE // 2, BOARD_SIZE))
BOTTOM_CUPS = list(range(BOARD_SIZE // 2))
ALL_CUPS = (BOTTOM_CUPS, TOP_CUPS)

PLAYER_1_CUPS = ('A', 'B', 'C', 'D', 'E', 'F')  # bottom
PLAYER_2_CUPS = ('f', 'e', 'd', 'c', 'b', 'a')  # top
ALL_PLAYERS_CUPS = (PLAYER_1_CUPS, PLAYER_2_CUPS)


class InvalidCup(Exception):
    pass


class InvalidSown(Exception):
    pass


class GameEnd(Exception):
    pass


class Awale(object):
    def __init__(self, board=None, scores=None):
        if scores is None:
            scores = [0, 0]
        if board is None:
            board = [INITIAL_SEEDS] * BOARD_SIZE
        self.board = board
        self.scores = scores
        self.current_turn = 0

    def __repr__(self):
        """Friendly representation of Awale data: board + score."""
        return "{'board': %s, 'scores': %s}" % (str(self.board), str(self.scores))

    def __str__(self):
        """Display Awale board."""
        s = ""
        # Top player (computer)
        s += "             "
        s += "    ".join([c for c in reversed(PLAYER_2_CUPS[:BOARD_SIZE // 2])])
        s += "\n"
        s += "Top (%2d) " % self.scores[TOP_PLAYER]
        s += " ".join(["[%2d]" % i for i in reversed(self.board[-BOARD_SIZE // 2:])])
        s += "\n"

        # Bottom player (player)
        s += "           "
        s += " ".join(["[%2d]" % i for i in self.board[:BOARD_SIZE // 2]])
        s += " (%2d) Bottom" % self.scores[BOTTOM_PLAYER]
        s += "\n"
        s += "             "
        s += "    ".join([c for c in PLAYER_1_CUPS[:BOARD_SIZE // 2]])

        return s

    def _identify_player_id(self, cup_index):
        """Determine who plays given index."""
        if cup_index in range(len(self.board)):
            if cup_index in BOTTOM_CUPS:
                return BOTTOM_PLAYER
            elif cup_index in TOP_CUPS:
                return TOP_PLAYER
        else:
            raise InvalidCup("Bad index given")

    def _check_move_validity(self, cup_index):
        """Check if sowing seeds from given index is authorized."""
        if cup_index in range(len(self.board)):
            # Cup must not be empty
            if self.board[cup_index] == 0:
                return False
            player = self._identify_player_id(cup_index)

            # Must feed the opponent
            if max([self.board[i] for i in ALL_CUPS[1 - player]]) == 0:
                status = False  # feed the opponent?
                nb_seeds = self.board[cup_index]
                i = 0
                while nb_seeds:
                    if (cup_index + i + 1) % len(self.board) == cup_index:
                        pass  # ignore departure cup
                    else:
                        if (cup_index + i + 1) % len(self.board) in ALL_CUPS[1 - player]:
                            status = True
                            break  # authorized, move feed the opponent
                        nb_seeds -= 1
                    i += 1

                if not status:  # unauthorized, move must feed him!
                    return False
            return True  # authorized, everything's okay
        else:
            raise InvalidCup("Bad index given")

    def _sow(self, cup_index):
        """Sow seeds from given cup index counter-clockwise.
        Update board cups and return the last position
        """
        if cup_index in range(len(self.board)):
            if not self._check_move_validity(cup_index):
                raise InvalidSown("Invalid cup")

            # First, get seeds from cup index
            nb_seeds = self.board[cup_index]
            self.board[cup_index] = 0  # empty cup

            # Then, sow seeds counter-clockwise
            i = 0
            while nb_seeds:
                if (cup_index + i + 1) % len(self.board) == cup_index:
                    pass  # do not give seed to departure cup!
                else:
                    self.board[(cup_index + i + 1) % len(self.board)] += 1
                    nb_seeds -= 1
                i += 1

            return (cup_index + i) % len(self.board)  # last cup position
        else:
            raise InvalidCup("Bad index given")

    def _capture(self, cup_index):
        """Capture seeds from given index clockwise.
        Update board cups and player score.
        Return the number of seeds captured."""

        if cup_index in range(len(self.board)):
            side = self._identify_player_id(cup_index)  # determine side

            board_copy = self.board[:]  # copy current board
            nb_seeds = 0
            # Try capturing seeds clockwise
            for i in range((cup_index % (len(self.board) // 2)) + 1):
                if 2 <= self.board[cup_index - i] <= 3:
                    nb_seeds += self.board[cup_index - i]
                    self.board[cup_index - i] = 0
                else:
                    break  # stop here

            # World Oware Federation rule : no capture when move will starve opponent
            if sum([self.board[idx] for idx in ALL_CUPS[side]]) == 0:  # not starve opponent
                self.board = board_copy[:]  # restore initial board
                nb_seeds = 0

            return nb_seeds
        else:
            raise InvalidCup("Bad index given")

    def _check_end(self):
        """Check if end game is reached"""

        # 25 seeds captured
        if self.scores[BOTTOM_PLAYER] > (len(self.board) * INITIAL_SEEDS) // 2:
            raise GameEnd("Bottom player wins!")
        elif self.scores[TOP_PLAYER] > (len(self.board) * INITIAL_SEEDS) // 2:
            raise GameEnd("Top player wins!")
        # No enough seeds
        if sum(self.board) < 6:
            raise GameEnd("No enough seeds!")
        return False

    def sow_and_capture(self, cup_index):
        """Sow seeds from given index counter-clockwise; capture seeds clockwise if possible.
        Update scores and return game status."""

        if cup_index in range(len(self.board)):
            player = self._identify_player_id(cup_index)  # determine who is playing
            # Sowing ...
            capt_idx = self._sow(cup_index)
            # Capturing ...
            if capt_idx in ALL_CUPS[1 - player]:  # if opponent cups
                self.scores[player] += self._capture(capt_idx)

            self.current_turn += 1  # increment turns counter

            # Check game end
            self._check_end()  # raise GameEnd error at end
        else:
            raise InvalidCup("Bad index given")
