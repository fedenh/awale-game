import os
import sys

import pygame
from pygame.locals import *

import awale

pygame.init()

START, PLAYER_1_TURN, PLAYER_2_TURN, GAME_END = list(range(4))
FPS = 50
BLACK_COLOR = (0, 0, 0)
WHITE_COLOR = (255, 255, 255)
BEIGE_COLOR = (245, 245, 220)
BROWN_COLOR = (88, 47, 27)
DISPLAY_WIDTH, DISPLAY_HEIGHT = 860, 600
PLAYER_CURSOR_SIZE = (50, 50)


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


def sow(awale_board: awale.Awale, player, cup):
    if cup not in (awale.ALL_PLAYERS_CUPS[player])[:len(awale_board.board) // 2]:
        raise awale.InvalidSown("Invalid cup")
    cup_index = list(awale.ALL_PLAYERS_CUPS[player]).index(cup)
    if player == awale.TOP_PLAYER:
        cup_index += 6
    try:
        awale_board.sow_and_capture(cup_index)
    finally:
        pass

    return cup_index


def close():
    sys.exit()


class CupSprite(pygame.sprite.Sprite):
    top_cups_positions = [
        (720, 200),
        (580, 200),
        (440, 200),
        (300, 200),
        (160, 200),
        (20, 200),
    ]

    bottom_cups_positions = [
        (20, 400),
        (160, 400),
        (300, 400),
        (440, 400),
        (580, 400),
        (720, 400),
    ]

    def __init__(self, cup, seeds_count=4):
        super().__init__()
        self.seeds_count = seeds_count
        if self.seeds_count < 8:
            self.image = pygame.image.load(resource_path("assets/board/%d_seeds.jpg" % self.seeds_count))
            self.image.set_colorkey(WHITE_COLOR)
            self.image = self.image.convert_alpha()
        else:
            self.image = pygame.image.load(resource_path("assets/board/8+_seeds.jpg"))
            self.image.set_colorkey(WHITE_COLOR)
            self.image = self.image.convert_alpha()
        self.rect = self.image.get_rect()
        self.cup_index = None
        if cup in awale.PLAYER_1_CUPS:
            self.cup_index = list(awale.PLAYER_1_CUPS).index(cup)
            self.rect.midleft = CupSprite.bottom_cups_positions[self.cup_index]
        elif cup in awale.PLAYER_2_CUPS:
            self.cup_index = list(awale.PLAYER_2_CUPS).index(cup) + awale.BOARD_SIZE // 2
            self.rect.midleft = CupSprite.top_cups_positions[self.cup_index - awale.BOARD_SIZE // 2]

        self.font = pygame.font.SysFont("Verdana", 16)
        self.font.set_italic(True)
        self.cup_text = self.font.render(cup, True, BLACK_COLOR, WHITE_COLOR)
        self.image.blit(self.cup_text, (0, 0))

    def update(self, board, count_seeds=False):
        self.seeds_count = board[self.cup_index]
        if self.seeds_count < 8:
            self.image = pygame.image.load(resource_path("assets/board/%d_seeds.jpg" % self.seeds_count)).copy()
            self.image.set_colorkey(WHITE_COLOR)
            self.image = self.image.convert_alpha()
        else:
            self.image = pygame.image.load(resource_path("assets/board/8+_seeds.jpg")).copy()
            self.image.set_colorkey(WHITE_COLOR)
            self.image = self.image.convert_alpha()

        if count_seeds:
            text = self.font.render("%d" % self.seeds_count, True, BLACK_COLOR, WHITE_COLOR)
            text_position = text.get_rect(topleft=(30, 30))
            self.image.blit(text, text_position)
        elif self.seeds_count > 7:
            text = self.font.render("%d" % self.seeds_count, True, BLACK_COLOR, WHITE_COLOR)
            text_position = text.get_rect(topleft=(30, 30))
            self.image.blit(text, text_position)

        self.image.blit(self.cup_text, (0, 0))


class HouseSprite(pygame.sprite.Sprite):
    """House sprite."""

    def __init__(self, player, nb_seeds=0):
        """Initialize house seeds for given player."""
        super().__init__()

        self.nb_seeds = nb_seeds
        self.player = player

        self.font = pygame.font.SysFont("Verdana", 16)
        self.text = self.font.render("%d" % self.nb_seeds, True, BROWN_COLOR, WHITE_COLOR)
        # Set the position
        self.rect = self.text.get_rect()
        if self.player == awale.BOTTOM_PLAYER:
            self.rect.midright = (DISPLAY_WIDTH - 10, DISPLAY_HEIGHT - 100)
        elif self.player == awale.TOP_PLAYER:
            self.rect.midright = (DISPLAY_WIDTH - 10, 100)

    def update(self, nb_seeds):
        """Update score according to nb_seeds."""
        self.nb_seeds = nb_seeds
        self.text = self.font.render("%d" % self.nb_seeds, True, BROWN_COLOR, WHITE_COLOR)


def awale_game():
    pygame.init()
    screen = pygame.display.set_mode((DISPLAY_WIDTH, DISPLAY_HEIGHT))
    pygame.display.set_caption("Awale")
    screen.fill(BEIGE_COLOR)

    welcome_image = pygame.image.load(resource_path("assets/welcome.png"))
    welcome_image = pygame.transform.smoothscale(welcome_image, screen.get_size())  # todo : change image
    background_image = pygame.image.load(resource_path("assets/background/wood.jpg"))
    # background_image = pygame.image.load(resource_path("assets/background/art.jpg"))
    background_image = pygame.transform.smoothscale(background_image, screen.get_size())
    player_1_cursor = pygame.image.load(resource_path("assets/chooser/bottom_hand.png")).convert_alpha()
    player_1_cursor = pygame.transform.smoothscale(player_1_cursor, PLAYER_CURSOR_SIZE)
    player_2_cursor = pygame.image.load(resource_path("assets/chooser/top_hand.png")).convert_alpha()
    player_2_cursor = pygame.transform.smoothscale(player_2_cursor, PLAYER_CURSOR_SIZE)
    font = pygame.font.SysFont("Verdana", 13)
    big_font = pygame.font.SysFont("Verdana", 24)

    cups = (
        CupSprite('A'),
        CupSprite('B'),
        CupSprite('C'),
        CupSprite('D'),
        CupSprite('E'),
        CupSprite('F'),

        CupSprite('f'),
        CupSprite('e'),
        CupSprite('d'),
        CupSprite('c'),
        CupSprite('b'),
        CupSprite('a'),
    )

    cups_group = pygame.sprite.RenderPlain(*cups)

    houses = (
        HouseSprite(awale.BOTTOM_PLAYER),
        HouseSprite(awale.TOP_PLAYER)
    )

    houses_group = pygame.sprite.RenderPlain(*houses)

    awale_board = awale.Awale()

    def _display_scores(game_state):
        """Helper sub function to display scores and turns."""
        if game_state == GAME_END:
            bottom_player_score, top_player_score = awale_board.scores  # final score
            # Count seeds left on board
            bottom_player_score += sum([awale_board.board[idx] for idx in awale.ALL_CUPS[awale.BOTTOM_PLAYER]])
            top_player_score += sum([awale_board.board[idx] for idx in awale.ALL_CUPS[awale.TOP_PLAYER]])

            if bottom_player_score > top_player_score:
                score_text = font.render(
                    "Bottom player wins (%d seeds vs %d)." % (bottom_player_score, top_player_score),
                    True, BROWN_COLOR, WHITE_COLOR)
            elif top_player_score > bottom_player_score:
                score_text = font.render(
                    "Top player wins (%d seeds vs %d)." % (top_player_score, bottom_player_score),
                    True, BROWN_COLOR, WHITE_COLOR)
            else:
                score_text = font.render("Draw (%d seeds each)." % bottom_player_score, True, BROWN_COLOR, WHITE_COLOR)
            score_text_position = score_text.get_rect(topleft=(0, 0))
            screen.blit(score_text, score_text_position)

            score_text = font.render("Replay by clicking or 'ESC' to quit.", True, BROWN_COLOR, WHITE_COLOR)
            score_text_position = score_text.get_rect(topleft=(0, 20))
            screen.blit(score_text, score_text_position)

        if game_state == PLAYER_1_TURN or game_state == PLAYER_2_TURN or game_state == GAME_END:
            score_text = font.render("Top player score: %d" % awale_board.scores[awale.TOP_PLAYER], True, BROWN_COLOR,
                                     WHITE_COLOR)
            score_text_position = score_text.get_rect(topleft=(0, 40))
            screen.blit(score_text, score_text_position)

            score_text = font.render("Bottom player score: %d" % awale_board.scores[awale.BOTTOM_PLAYER], True,
                                     BROWN_COLOR,
                                     WHITE_COLOR)
            score_text_position = score_text.get_rect(topleft=(0, 60))
            screen.blit(score_text, score_text_position)

            player_side = None
            if game_state == PLAYER_1_TURN:
                player_side = 'Bottom player'
            elif game_state == PLAYER_2_TURN:
                player_side = 'Top player'

            if player_side is not None:
                score_text = font.render("Player: " + player_side, True, BROWN_COLOR, WHITE_COLOR)
                score_text_position = score_text.get_rect(topleft=(0, 100))
                screen.blit(score_text, score_text_position)

        # Display turns
        score_text = font.render("Turn: %d" % awale_board.current_turn, True, BROWN_COLOR, WHITE_COLOR)
        score_text_position = score_text.get_rect(topleft=(0, 80))
        screen.blit(score_text, score_text_position)

    index_bottom = index_top = None

    state = START
    disp_help = False
    game_begins = False
    clock = pygame.time.Clock()
    while True:
        clock.tick(FPS)
        if state == START:
            pygame.mouse.set_visible(True)
            pygame.mouse.set_cursor(*pygame.cursors.arrow)

            for event in pygame.event.get():
                if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    sys.exit()  # end
                elif event.type == MOUSEBUTTONDOWN or (event.type == KEYDOWN and event.key == K_RETURN):
                    state = PLAYER_1_TURN
                else:
                    if event.type == KEYDOWN:
                        if event.key == K_h or event.key == K_QUESTION:  # display help?
                            if not disp_help:
                                disp_help = True
                            else:
                                disp_help = False

            index_bottom = index_top = None
            awale_board.board = [awale.INITIAL_SEEDS] * awale.BOARD_SIZE
            awale_board.scores = [0, 0]
            awale_board.current_turn = 0

            screen.blit(background_image, (0, 0))
            text = big_font.render("Ready ? Click to start game", True, BROWN_COLOR, BEIGE_COLOR)
            text_position = text.get_rect(topleft=(110, 400))
            welcome_image.blit(text, text_position)

            text = big_font.render("Credits", True, BLACK_COLOR, WHITE_COLOR)
            text_position = text.get_rect(topleft=(110, 445))
            welcome_image.blit(text, text_position)

            text = font.render("Group 2 - Python & R Course / M1 SIRI / IFRI-UAC / 2022-2023 ", True, WHITE_COLOR,
                               BROWN_COLOR)
            text_position = text.get_rect(topleft=(110, 480))
            welcome_image.blit(text, text_position)

            text = font.render("  - Eden Houndonougbo               ", True, WHITE_COLOR, BROWN_COLOR)
            text_position = text.get_rect(topleft=(110, 495))
            welcome_image.blit(text, text_position)

            text = font.render("  - François Dossou                     ", True, WHITE_COLOR, BROWN_COLOR)
            text_position = text.get_rect(topleft=(110, 510))
            welcome_image.blit(text, text_position)

            text = font.render("  - Abdoul-Karim Mahoulikponto    ", True, WHITE_COLOR, BROWN_COLOR)
            text_position = text.get_rect(topleft=(110, 525))
            welcome_image.blit(text, text_position)

            screen.blit(welcome_image, (0, 0))
        elif state == PLAYER_1_TURN or state == PLAYER_2_TURN:
            pygame.mouse.set_visible(False)
            if state == PLAYER_1_TURN:
                screen.blit(player_1_cursor, pygame.mouse.get_pos())
                for event in pygame.event.get():
                    if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                        state = GAME_END  # end
                    else:
                        if event.type == KEYDOWN:
                            if event.key == K_h or event.key == K_QUESTION:  # display help?
                                if not disp_help:
                                    disp_help = True
                                else:
                                    disp_help = False
                            elif event.key == K_c and game_begins is False:
                                state = PLAYER_2_TURN
                                continue

                        if event.type == MOUSEBUTTONDOWN:
                            game_begins = True
                            cup = None
                            if cups[awale.PLAYER_1_CUPS.index('A')].rect.collidepoint(event.pos):
                                cup = 'A'
                            elif cups[awale.PLAYER_1_CUPS.index('B')].rect.collidepoint(event.pos):
                                cup = 'B'
                            elif cups[awale.PLAYER_1_CUPS.index('C')].rect.collidepoint(event.pos):
                                cup = 'C'
                            elif cups[awale.PLAYER_1_CUPS.index('D')].rect.collidepoint(event.pos):
                                cup = 'D'
                            elif cups[awale.PLAYER_1_CUPS.index('E')].rect.collidepoint(event.pos):
                                cup = 'E'
                            elif cups[awale.PLAYER_1_CUPS.index('F')].rect.collidepoint(event.pos):
                                cup = 'F'

                            if cup:
                                try:
                                    index_bottom = sow(awale_board, awale.BOTTOM_PLAYER, cup)
                                except awale.InvalidSown as msg:  # invalid input
                                    if __debug__:
                                        print(msg, file=sys.stderr)
                                    pass
                                except awale.GameEnd:  # eog
                                    state = GAME_END  # end
                                else:
                                    state = PLAYER_2_TURN

            elif state == PLAYER_2_TURN:
                screen.blit(player_2_cursor, pygame.mouse.get_pos())
                for event in pygame.event.get():
                    if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                        state = GAME_END  # end
                    else:
                        if event.type == KEYDOWN:
                            if event.key == K_h or event.key == K_QUESTION:  # display help?
                                if not disp_help:
                                    disp_help = True
                                else:
                                    disp_help = False
                            elif event.key == K_c and game_begins is False:
                                state = PLAYER_1_TURN
                                continue

                        if event.type == MOUSEBUTTONDOWN:
                            game_begins = True
                            cup = None
                            if cups[awale.PLAYER_2_CUPS.index('a') + 6].rect.collidepoint(event.pos):
                                cup = 'a'
                            elif cups[awale.PLAYER_2_CUPS.index('b') + 6].rect.collidepoint(event.pos):
                                cup = 'b'
                            elif cups[awale.PLAYER_2_CUPS.index('c') + 6].rect.collidepoint(event.pos):
                                cup = 'c'
                            elif cups[awale.PLAYER_2_CUPS.index('d') + 6].rect.collidepoint(event.pos):
                                cup = 'd'
                            elif cups[awale.PLAYER_2_CUPS.index('e') + 6].rect.collidepoint(event.pos):
                                cup = 'e'
                            elif cups[awale.PLAYER_2_CUPS.index('f') + 6].rect.collidepoint(event.pos):
                                cup = 'f'

                            if cup:
                                try:
                                    index_top = sow(awale_board, awale.TOP_PLAYER, cup)
                                except awale.InvalidSown as msg:  # invalid input
                                    if __debug__:
                                        print(msg, file=sys.stderr)
                                    pass
                                except awale.GameEnd:  # eog
                                    state = GAME_END  # end
                                else:
                                    state = PLAYER_1_TURN  # next= computer turn

                                pygame.event.clear()

            cups_group.update(awale_board.board)

            for player in [awale.BOTTOM_PLAYER, awale.TOP_PLAYER]:
                houses[player].update(awale_board.scores[player])
                pass

            screen.blit(background_image, (0, 0))
            cups_group.draw(screen)
            for house in houses_group:
                screen.blit(house.text, house.rect)

            if index_bottom in awale.BOTTOM_CUPS:
                text = font.render("Player 1 last move: '%s'" % (awale.PLAYER_1_CUPS[index_bottom]), True, BROWN_COLOR,
                                   WHITE_COLOR)
                text_position = text.get_rect(topleft=(0, 0))
                screen.blit(text, text_position)

            if index_top in awale.TOP_CUPS:
                text = font.render("Player 2 last move: '%s'" % (awale.PLAYER_2_CUPS[index_top - 6]), True, BROWN_COLOR,
                                   WHITE_COLOR)
                text_position = text.get_rect(topleft=(0, 20))
                screen.blit(text, text_position)

            _display_scores(state)

            if state == PLAYER_1_TURN:
                screen.blit(player_1_cursor, pygame.mouse.get_pos())
            elif state == PLAYER_2_TURN:
                screen.blit(player_2_cursor, pygame.mouse.get_pos())

        elif state == GAME_END:
            pygame.mouse.set_visible(True)
            pygame.mouse.set_cursor(*pygame.cursors.broken_x)  # end cursor

            for event in pygame.event.get():
                if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    sys.exit()  # exit application

                elif event.type == MOUSEBUTTONDOWN or (
                        event.type == KEYDOWN and (event.key == K_RETURN or event.key == K_c)):
                    state = START  # next= replay
                    game_begins = False
                else:
                    if event.type == KEYDOWN:
                        if event.key == K_h or event.key == K_QUESTION:  # display help?
                            if not disp_help:
                                disp_help = True
                            else:
                                disp_help = False

            # Update all cups (for last turn) according to Awale board
            cups_group.update(awale_board.board)

            # Update houses
            for player in [awale.BOTTOM_PLAYER, awale.TOP_PLAYER]:
                houses[player].update(awale_board.scores[player])
                pass

            # Rendering board
            screen.blit(background_image, (0, 0))
            cups_group.draw(screen)
            for house in houses_group:
                screen.blit(house.text, house.rect)

            # Display Top/Bottom scores + current turn
            _display_scores(state)

        pygame.display.flip()


if __name__ == "__main__":
    awale_game()
